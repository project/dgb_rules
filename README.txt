// $Id$

Drupal Guestbook Rules (DGBR) provides Rules module integration for the
Drupal Guestbook (DGB) module.

Requirements
--------------------------------------------------------------------------------
- This module is written for Drupal 6.0+.
- The Rules module.
- The Drupal Guestbook (DGB) module.

Installation
--------------------------------------------------------------------------------
Copy the DGBR module folder to your module directory and then enable on the
admin modules page.
