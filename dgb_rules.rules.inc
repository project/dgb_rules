<?php
// $Id$

/**
 * @file
 * Hooks and callback functions for rules.module integration.
 */

/**
 * Implements hook_rules_action_info().
 *
 * Trigger events when:
 * 1) DGB entry is inserted, updated or deleted
 * 2) DGB entry comment is inserted, updated or deleted
 *
 * @TODO
 * event for publishing/unpublish of entry
 * events for entry comments
 *
 * @ingroup rules
 */
function dgb_rules_rules_event_info() { 
  $entry_arguments = array(
    'entry' => array(
      'type' => 'entry',
      'label' => t('DGB Entry'),
      'description' => t('The Drupal Guestbook entry')
    ),
    'entry_author' => array(
      'type' => 'user',
      'label' => t('DGB Entry Author'),
      'description' => t('The author of the Drupal Guestbook entry.'),
      'handler' => 'rules_events_argument_dgb_entry_author',
    ),
    'entry_recipient' => array(
      'type' => 'user',
      'label' => t('DGB Entry Recipient'),
      'description' => t('The recipient of the Drupal Guestbook entry.'),
      'handler' => 'rules_events_argument_dgb_entry_recipient',
    ),      
  ) + rules_events_global_user_argument();  
  return array(
    //DGB Entry
    'rules_event_dgb_rules_entry_insert' => array(
      'label' => t('DGB Entry Inserted'),
      'module' => 'DGB Rules',
      'arguments' => $entry_arguments,
    ),
    'rules_event_dgb_rules_entry_update' => array(
      'label' => t('DGB Entry Updated'),
      'module' => 'DGB Rules',
      'arguments' => $entry_arguments,
    ),
    'rules_event_dgb_rules_entry_delete' => array(
      'label' => t('DGB Entry Deleted'),
      'module' => 'DGB Rules',
      'arguments' => $entry_arguments,
    ),

    /*DGB Entry Comment
    'rules_event_dgb_rules_entry_comment_insert' => array(
      'label' => t('DGB Entry Comment Inserted'),
      'module' => 'DGB Rules',
      'arguments' => $arguments,
    ),
    'rules_event_dgb_rules_entry_comment_update' => array(
      'label' => t('DGB Entry Comment Updated'),
      'module' => 'DGB Rules',
      'arguments' => $arguments,
    ),
    'rules_event_dgb_rules_entry_comment_delete' => array(
      'label' => t('DGB Entry Comment Deleted'),
      'module' => 'DGB Rules',
      'arguments' => $arguments,
    ),  */    
  );
}

/**
 * Get entry author as rules argument
 */
function rules_events_argument_dgb_entry_author($entry) {
  if (!empty($entry['author_obj'])) {
    return $entry['author_obj'];
  }
  else {
    $uid = $entry['author'];
    return user_load($uid);
  }
}

/**
 * Get entry recipient as rules argument
 */
function rules_events_argument_dgb_entry_recipient($entry) {
  if (!empty($entry['recipient_obj'])) {
    return $entry['recipient_obj'];
  }
  else {
    $uid = $entry['recipient'];
    return user_load($uid);
  }
}

/**
 * Implementation of hook_rules_condition_info().
 *
 * @ingroup rules
 */
function dgb_rules_rules_condition_info() {
  return array(
    'rules_condition_dgb_rules_entry_ispublished' => array(
      'label' => t('Is DGB entry published'),
      'arguments' => array(
        'entry' => array('type' => 'entry', 'label' => t('DGB Entry')),    
       ),
      'help' => t('Evaluates to TRUE if a DGB entry is published.'),
      'module' => 'DGB Rules',
    ),
  );
}

/**
 * Condition callback to determine if entry is published
 */
function rules_condition_dgb_rules_entry_ispublished(&$entry, $settings) {
  //0 = published, 1 = unpublished
	return $entry['status'] == DGB_ENTRY_PUBLISHED;
}

/**
 * Implementation of hook_rules_data_type_info().
 */
function dgb_rules_rules_data_type_info() {
  return array(
    'entry' => array(
      'label' => t('DGB Entry'),
      'class' => 'rules_data_type_dgb_entry',
      'savable' => FALSE,
      'identifiable' => TRUE,
      'module' => 'DGB Rules',
    ),
  );
}

/**
 * Defines the comment data type
 */
class rules_data_type_dgb_entry extends rules_data_type {
  function load($id) {
    return dgb_entry_load($id);
  }

  function get_identifier() {
    $entry = &$this->get();
    return $entry['id'];
  }
}

/**
 * @}
 */